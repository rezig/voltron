import React, { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { useDispatch } from 'react-redux'

import { login } from '../../actions/auth'
import { store } from '../../store'

export function SignIn() {

  const [infos, setInfos] = useState({
    username: 'Jean',
    password: '12345',
  })
  const isAuthenticated = store.getState().auth.isAuthenticated

  const { username, password } = infos;
  const dispatch = useDispatch()

  function onSubmit() {
    dispatch(login(username, password))
  } 

  function onChangeHandle(e) {
    const { name, value } = e.target;
    setInfos(infos => ({ ...infos, [name]: value }));
  }
  if (isAuthenticated) {
    return <Redirect to="/" />;
  }
  return (
    <div className="col-md-6 m-auto">
        <div className="card card-body mt-5">
          <h2 className="text-center">Sign In</h2>
          <div > 
            <div className="form-group">
              <label>Username</label>
              <input
                type="text"
                className="form-control"
                name="username"
                onChange={onChangeHandle}
                value={username || ""}
              />
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                className="form-control"
                name="password"
                onChange={onChangeHandle}
                value={password || ""}
              />
            </div>

            <div className="form-group">
              <button onClick={onSubmit} className="btn btn-primary">
                Login
              </button>
            </div>
            <p>
              Don't have an account? <Link to="/signup">Sign Up</Link>
            </p>
          </div>
        </div>
      </div>
  )
}
