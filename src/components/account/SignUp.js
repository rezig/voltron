import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'

import { register } from '../../actions/auth'

export function SignUp() {

  const [infos, setInfos] = useState({
    username: 'Besma',
    password: '12345',
    password2: '12345',
  })

  const { username, password, password2 } = infos;
  const dispatch = useDispatch()

  function onSubmit() {
    dispatch(register(username, password))
  }

  function onChangeHandle(e) {
    const { name, value } = e.target;
    setInfos(infos => ({ ...infos, [name]: value }));
  }

  return (
    <div className="col-md-6 m-auto">
        <div className="card card-body mt-5">
          <h2 className="text-center">Sign Up</h2>
          <div> 
            <div className="form-group">
              <label>Username</label>
              <input
                type="text"
                className="form-control"
                name="username"
                onChange={onChangeHandle}
                value={username || ""}
              />
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                className="form-control"
                name="password"
                onChange={onChangeHandle}
                value={password || ""}
              />
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                className="form-control"
                name="password"
                onChange={onChangeHandle}
                value={password2 || ""}
              />
            </div>

            <div className="form-group">
              <button onClick={onSubmit} className="btn btn-primary">
                Sign Up
              </button>
            </div>
            <p>
              Already have an account? <Link to="/signin">Sign In</Link>
            </p>
          </div>
        </div>
      </div>
  )
}
