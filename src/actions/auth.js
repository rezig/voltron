import axios from 'axios'

import { LOGIN_SUCCESS, LOGIN_FAIL, REGISTER_SUCCESS , REGISTER_FAIL } from './types'
import { store } from '../store'


// LOGIN USER
export const login = (username, password) => (dispatch) => {
  // Headers
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }

  // Request Body  
  const body = JSON.stringify({"user": { username, password }})
  const isAuthenticated = store.getState().auth.token

  axios
    .post('http://localhost:3001/users/signin', body, config)
    .then((res) => {
      console.log(isAuthenticated)
      dispatch({
        type: LOGIN_SUCCESS,
        payload: 
        {
          token: res.data.access_token,
          user: res.data.user
        },
      })
    })
    .catch((err) => {
      console.warn(err)
      dispatch({
        type: LOGIN_FAIL,
      })
    })
}


// REGISTER USER
export const register = (username, password) => (dispatch) => {
  // Headers
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }
  // Request Body  
  const body = JSON.stringify({"user": { username, password }})

  axios
    .post('http://localhost:3001/users/signup', body, config)
    .then((res) => {
      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data,
      })
    })
    .catch((err) => {
      console.warn(err)
      dispatch({
        type: REGISTER_FAIL,
      })
    })
}